#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def magicspow5():
    maxi = 5*9**5
    for i in xrange(2, maxi):
        if i == sum(int(d)**5 for d in str(i)):
            yield i

print sum(magicspow5())
