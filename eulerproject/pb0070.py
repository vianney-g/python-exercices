#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def primes():
    n = 2
    tested = {}
    while 1:
        if n not in tested:
            yield n
            tested[n*n] = [n,]
        else:
            for t in tested[n]:
                tested.setdefault(t+n, []).append(t)
            del tested[n]
        n += 1

ispermutation = lambda n, m: sorted(str(n)) == sorted(str(m))

res=[]
for p in primes():
    for q in primes():
        n = p*q
        if n > 10000000:
            break
        phi = (p - 1)*(q - 1)
        if ispermutation(n,phi):
            print n, phi, float(n)/phi
            res.append((float(n)/phi, phi, n))
    if p > 5000000:
        break

print min(res)

