#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def memo(f):
    class Memo(dict):
        def __missing__(self, key):
            r = self[key] = f(key)
            return r
    return Memo().__getitem__

@memo
def factors(n):
    for d in xrange(2, n/2 + 1):
        if n % d == 0:
            pre_fact = factors(n/d)
            return pre_fact.union(d*a for a in pre_fact)
    return set((1,n))


def abundants(limit=28124):
    n = 12
    while n < limit:
        if sum(factors(n)) - n > n:
            yield n
        n += 1

a_sums = set()
a_collect = []
for a in abundants():
    a_collect.append(a)
    a_sums = a_sums.union(a + ac for ac in a_collect if a + ac < 28124)

print sum(n for n in xrange(28124) if n not in a_sums)

