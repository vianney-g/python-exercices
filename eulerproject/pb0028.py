#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def spiral_diag_val(spiralsize=1001):
    offset, currentsize = 1, 0
    yield offset
    while currentsize < spiralsize - 1:
        offset += 1                 # a small step right
        currentsize += 2
        offset += currentsize  - 1  # move down
        yield offset
        offset += currentsize       # move left
        yield offset
        offset += currentsize       # move up
        yield offset
        offset += currentsize       # move right
        yield offset

print sum(d for d in spiral_diag_val(1001))
