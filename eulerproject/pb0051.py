#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from time import time

start = time()


def memo(f):
    class Memo(dict):
        def __missing__(self, k):
            r = self[k] = f(k)
            return r
    return Memo().__getitem__

def primes():
    yield 1
    n = 2
    tested = {}
    while 1:
        if n not in tested:
            yield n
            tested[n*n] = [n,]
        else:
            for t in tested[n]:
                tested.setdefault(t+n, []).append(t)
            del tested[n]
        n += 1

@memo
def isprime(n):
    if n%2 == 0:
        return False
    for d in xrange(3, int(n**0.5)+1, 2):
        if n%d == 0:
            return False
    return True

def digitsreplacement(n):
    n = str(n)
    for d in '0123456789':
        if n[1:-1].count(d) > 1:
            yield [int(n[:-1].replace(d, d2) + n[-1]) for d2 in '0123456789']

def getsmallestone(primenb):
    for n in primes():
        for dr in digitsreplacement(n):
            if len([k for k in dr if isprime(k) and len(str(k)) == len(str(n))]) >= primenb:
                return n

print getsmallestone(8)
print time() - start
