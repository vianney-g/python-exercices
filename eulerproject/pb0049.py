#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com
"""
from itertools import combinations_with_replacement, permutations

def isprime(n):
    for d in xrange(2, int(n**0.5)+1):
        if not n%d:
            return False
    return True

def common_dif(perm):
    for p in perm:
        diffs = [abs(p - k) for k in perm]
        for d in diffs:
            if diffs.count(d) > 1:
                return p-d, p, p+d

def strangesuites():
    for n in combinations_with_replacement('1234567890', 4):
        perm = {int(''.join(k)) for k in permutations(n)}
        perm = [k for k in perm if k > 999 and isprime(k)]
        if len(perm) > 2:
            yield common_dif(perm)

print [s for s in strangesuites() if s]

