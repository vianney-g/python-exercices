#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from itertools import permutations

def valid_rings():
    ring_extern = (0, 1, 2, 3, 4)
    ring_lines_idx = ((0,6,7), (1,7,8), (2,8,9), (3,9,5), (4,5,6))
    for ring in permutations(range(1,11)):
        if min(ring[i] for i in ring_extern) != ring[0]:
            continue
        ring_lines = [[ring[i] for i in line] for line in ring_lines_idx]
        sums = {sum(line) for line in ring_lines}
        if len(sums) == 1:
            yield ring_lines

len16rings=[]
for ring in valid_rings():
    r2s = ''.join(''.join(str(s) for s in rline) for rline in ring)
    if len(r2s) == 16:
        len16rings.append(r2s)

print max(len16rings)

