#!/usr/bin/env python
# *-* coding:utf-8 *-*

positions = {str(i): [] for i in xrange(10)}
with open('pb0079.txt') as f:
    for s in f.readlines():
        for c in s.strip():
            positions[c].append(s.index(c))

avg = [(sum(p)/float(len(p)), n) for n, p in positions.items() if p]
print ''.join(x[1] for x in sorted(avg))
