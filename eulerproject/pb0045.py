#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def pentagonals():
    n = 0
    while 1:
        n += 1
        yield n*(3*n-1)/2

def hexagonals():
    n = 0
    while 1:
        n += 1
        yield n*(2*n-1)

def multigonals():
    ptgs, p = pentagonals(), 0
    for h in hexagonals():
        while p < h:
            p = ptgs.next()
        if h == p:
            yield h

mlgs = multigonals()
print [mlgs.next() for _ in xrange(3)]
