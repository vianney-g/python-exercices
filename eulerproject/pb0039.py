#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def triangles(p):
    """get triangle for the given perimeter"""
    for a in xrange(1,p):
        for b in xrange(a,p):
            c = p - a - b
            if c*c == a*a + b*b:
                yield (a, b, c)

trinb = []

for p in xrange(2, 1001):
    lenp = len([t for t in triangles(p)])
    print p, lenp
    trinb.append((lenp, p))

print max(trinb)
#print max([(len([t for t in triangles(p)]), p) for p in xrange(2, 1001)])
