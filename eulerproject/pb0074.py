#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from math import factorial
from time import time

start = time()

digits_factorial_sum = lambda n: sum(factorial(int(d)) for d in str(n))

class FactorialLoopsBuilder(dict):
    def __missing__(self, k):
        r = self[k] = digits_factorial_sum(k)
        return r

    def __call__(self, n):
        res = [n,]
        n = self[n]
        while n not in res:
            res.append(n)
            n= self[n]
        return res


flb = FactorialLoopsBuilder()
print sum(1 for n in xrange(1000000) if len(flb(n)) == 60)

