#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""


def powern_digitsn(n):
    i = powerlen = 1
    while powerlen <= n:
        if powerlen == n:
            yield i**n
        i += 1
        powerlen = len(str(i**n))

res, n = [], 1
while 1:
    pdn = [k for k in powern_digitsn(n)]
    if not pdn:
        break
    res.extend(pdn)
    n += 1

print len(res)
