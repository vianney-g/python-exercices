#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def fibo():
    a = 1
    b = 2
    yield a
    yield b
    while True:
        c = a+b
        a = b
        b = c
        yield c

sum = 0
for n in fibo():
    if n >= 4000000:
        break
    if n % 2 == 0:
        sum += n

print sum
