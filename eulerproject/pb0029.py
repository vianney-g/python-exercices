#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def powersequence():
    ps = set(())
    for a in xrange(2, 101):
        for b in xrange(2, 101):
            ps.add(a**b)
    return ps

print len(powersequence())
