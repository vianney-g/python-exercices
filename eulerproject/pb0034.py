#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from math import factorial

def curiousnbs():
    for i in xrange(3, 100000):
        if i == sum(factorial(int(d)) for d in str(i)):
            yield i

print sum(curiousnbs())
