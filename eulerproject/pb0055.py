#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

reverse = lambda n: int(str(n)[::-1])
ispalindrome = lambda n: n == reverse(n)

def islychrel(n):
    for _ in xrange(50):
        n += reverse(n)
        if ispalindrome(n):
            return False
    return True

print sum(1 for n in xrange(1, 10000) if islychrel(n))
