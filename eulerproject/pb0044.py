#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def pentagonals():
    n = 0
    while 1:
        n += 1
        yield n*(3*n - 1)/2

ispentagonal = lambda n: not (1+(1+24*n)**0.5)%6

def ptgpair_mindiff():
    ptgs = []
    for p in pentagonals():
        for p2 in ptgs[::-1]:
            if ispentagonal(p-p2) and ispentagonal(p+p2):
                return (p, p2)
        ptgs.append(p)

a, b = ptgpair_mindiff()
print '%i - %i = %i'%(a, b, a-b)
