#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from time import time

start = time()

def squareroot_period(n):
    a0 = int(n**0.5)
    yield a0
    if a0*a0 == n:
        return
    m, d, a = 0, 1, a0
    triplet_history = []
    while 1:
        m2 = d*a - m
        d2 = (n - m2**2)/d
        a2 = (a0 + m2)/d2
        if (m2, d2, a2) in triplet_history:
            break
        m, d, a = m2, d2, a2
        triplet_history.append((m , d, a))
        yield a


def continued_approximation(sequence):
    h1, h2, k1, k2 = 1, 0, 0, 1
    for a in sequence:
        h1, h2 = a*h1 + h2, h1
        k1, k2 = a*k1 + k2, k1
    return h1, k1


def diophantine_xy(D):
    seq = [a for a in squareroot_period(D)]
    if len(seq) == 1:
        return None
    seq = seq[:-1] if len(seq) % 2 else seq + seq[1:-1]
    return continued_approximation(seq)

print max((diophantine_xy(D), D) for D in range(2, 1001))
print time() - start
