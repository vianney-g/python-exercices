#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from itertools import cycle, izip, permutations, combinations_with_replacement

asciitext = lambda t: ''.join(chr(n) for n in t)
xortext = lambda t, key: [n^k for n,k in izip(t, cycle([ord(c) for c in key]))]

def crack(text):
    for letters in combinations_with_replacement('abcdefghijklmnopqrstuvwxyz', 3):
        for key in permutations(letters):
            if ' and ' in asciitext(xortext(text, key)):
                return asciitext(xortext(text, key))

with open('pb0059.txt') as f:
    codedtext = [int(n) for n in f.readline().split(',')]

print sum(ord(c) for c in crack(codedtext))
