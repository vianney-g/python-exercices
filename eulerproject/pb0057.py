#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from time import time

start = time()

def squareroot_fractions():
    h1, h2, k1, k2 = 1, 1, 1, 0
    while 1:
        h1, h2 = 2*h1 + h2, h1
        k1, k2 = 2*k1 + k2, k1
        yield h2, k2

big_numerator = lambda n, d: len(str(n)) > len(str(d))
sf = squareroot_fractions()

print sum(1 for _ in xrange(1000) if big_numerator(*sf.next()))

print time() - start
