#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from itertools import permutations

def factors(n):
    for d in xrange(2, int(n**0.5) + 1):
        if n % d == 0:
            pre_fact = factors(n/d)
            return pre_fact.union(d*a for a in pre_fact)
    return set((1,n))

def pandigits():
    digits = set('123456789')
    for p in permutations(digits, 4):
        rest = ''.join(sorted(digits - set(p)))
        n = int(''.join(p))
        divs = factors(n)
        while divs:
            d = divs.pop()
            dcat = ''.join(sorted(str(d) + str(n/d)))
            if dcat == rest:
                yield n

print sum(set(pandigits()))

