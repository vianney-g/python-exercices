#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

themax = 0
for a in xrange(1,101):
    for b in xrange(1,101):
        thesum = sum(int(c) for c in str(a**b))
        themax = thesum if thesum > themax else themax

print themax
