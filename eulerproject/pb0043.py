#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from itertools import permutations
from operator import and_

PROPS = (
    ((7,9), 17), # indices and divisor
    ((6,8), 13),
    ((5,7), 11),
    ((4,6), 7),
    ((3,5), 5),
    ((2,4), 3),
    ((1,3), 2),
)
propmet = lambda s, ind, div: int(s[ind[0]:ind[1]+1])%div == 0
isstrange = lambda s: reduce(and_, [propmet(s, ind, div) for ind, div in PROPS])

print sum([int(''.join(p)) for p in permutations('1234567890') if isstrange(''.join(p))])
