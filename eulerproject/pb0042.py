#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from string import uppercase

datas = ''
while True:
    try:
        datas += raw_input()
    except EOFError:
        break

datas = [word.strip()[1:-1] for word in datas.split(',')]

score = lambda word: sum(uppercase.index(c) + 1 for c in word)
istriangle = lambda n: not (-1 + (1+8*n)**0.5)%2

print sum(1 for word in datas if istriangle(score(word)))
