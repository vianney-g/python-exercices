#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def pythagore_tuples():
    def odds(limit=None):
        n = 1
        while (not limit) or n <= limit:
            yield n
            n += 2

    for k in odds():
        a = int(k**0.5)
        if a*a == k and a != 1:
            print a
            b = int(sum(odds(k-1))**0.5)
            c = int(sum(odds(k))**0.5)
            yield a, b, c

for a, b, c in pythagore_tuples():
    if c > 99:
        break
    print a, b, c

"""
limit = 1500000
pt = pythagore_tuples()
sieve = limit*[0,]
#L  = sum(pt.next())
a, b, c = pt.next()
L = sum((a, b, c))

while L <= 100*limit:
    step = L
    while 1:
        if L > limit:
            break
        sieve[L-1] += 1
        L += step
    a, b, c = pt.next()
    L = sum((a, b, c))
#    L = sum(pt.next())

print sieve[119]
print sum(1 for n in sieve if n == 1)
"""

