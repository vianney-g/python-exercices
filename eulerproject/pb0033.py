#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from fractions import Fraction
from itertools import chain
from operator import mul

def removesamedigits(a, b):
    if a/10 == b/10:
        return a%10, b%10
    if a%10 == b%10:
        return a/10, b/10
    if a%10 == b/10:
        return a/10, b%10
    if a/10 == b%10:
        return a%10, b/10


def curiousfracs():
    numerators = chain(*(xrange(k*10+1, (k+1)*10) for k in xrange(1,10)))
    def denominators(numerator):
        d = []
        d.append(xrange(numerator/10*10+1, numerator/10*10+10))
        d.append(xrange(numerator%10*10+1, numerator%10*10+10))
        d.append(xrange(10+numerator/10, 100, 10))
        d.append(xrange(10+numerator%10, 100, 10))
        return set(den for den in chain(*(x for x in d)) if den > numerator)

    for num in numerators:
        for den in denominators(num):
            if Fraction(num, den) == Fraction(*removesamedigits(num, den)):
                yield Fraction(num, den)

print reduce(mul, curiousfracs()).denominator
