#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
def memo(f):
    class Memo(dict):
        def __missing__(self, k):
            r = self[k] = f(k)
            return r
    return Memo().__getitem__

def factors(n):
    for d in xrange(2, int(n**0.5) + 1):
        if n % d == 0:
            pre_fact = factors(n/d)
            return pre_fact.union(d*a for a in pre_fact)
    return set((1,n))

@memo
def countit(n):
    return n - 1 - sum(countit(m) for m in list(sorted(factors(n)))[1:-1])

counter = 0
for d in xrange(2, 1000001):
    print d
    counter += countit(d)

print counter
