#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def isprime(n):
    for d in xrange(2, n/2 + 1):
        if n % d == 0:
            return False
    return True

def primes():
    n = 2
    while n:
        if isprime(n):
            yield n
        n += 1

p = primes()
for _ in xrange(10001):
    r = p.next()

print r

