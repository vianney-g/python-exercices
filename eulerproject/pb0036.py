#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

ispalindrome = lambda s: s == s[::-1]
is2base_pldm = lambda n: ispalindrome(str(n)) and ispalindrome(bin(n)[2:])

print sum(n for n in xrange(1,1000001) if is2base_pldm(n))
