#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from time import time

start = time()

class Hand(object):

    RANK = {c: value for value, c in enumerate('23456789TJQKA', 2)}

    def __init__(self, cards):
        self.values = [self.RANK[c[0]] for c in cards]
        self.colors = [c[1] for c in cards]
        self.pack = zip(self.values, self.colors)

    def pairs(self):
        """ return pairs values """
        return sorted(list({v for v in self.values if self.values.count(v) > 1}), reverse=True)

    def three(self):
        """ return three of a kind if found """
        t = [v for v in self.values if self.values.count(v) > 2]
        if t:
            return t[0], sorted([v for v in self.values if v != t[0]])
        return ()

    def four(self):
        """ return four of a kind if found """
        f = [v for v in self.values if self.values.count(v) > 3]
        if f:
            return f[0], [v for v in self.values if v != f[0]][0]
        return ()

    def straight(self):
        """ return straight start value if found else 0"""
        return min(self.values) if ''.join([str(v) for v in sorted(self.values)]) in '23456789TJQKA' else 0

    def isflush(self):
        return len(set(self.colors)) == 1

    def straightflush(self):
        return self.isflush() and self.straight()

    def fullhouse(self):
        """ return full values (three_value, pair_value) if found, empty tuple if not found"""
        t = self.three()
        if t:
            three = t[0]
            two = [v for v in self.pairs() if v != three]
            if two:
                return (three, two[0])
        return ()

    def __eq__(self, other):
        if sorted(self.values) == sorted(other.values):
            if self.isflush() == other.isflush():
                return True
        return False

    def __gt__(self, other):
        s_straightflush, o_straightflush = self.straightflush(), other.straightflush()
        s_fullhouse, o_fullhouse = self.fullhouse(), other.fullhouse()
        s_isflush, o_isflush = self.isflush(), other.isflush()
        s_straight, o_straight = self.straight(), other.straight()
        s_four, o_four = self.four(), other.four()
        s_three, o_three = self.three(), other.three()
        s_pairs, o_pairs = self.pairs(), other.pairs()

        if self == other:
            return False
        if s_straightflush > o_straightflush:
            return True
        if s_straightflush < o_straightflush:
            return False
        if s_four > o_four:
            return True
        if s_four < o_four:
            return False
        if s_fullhouse > o_fullhouse:
            return True
        if o_fullhouse > s_fullhouse:
            return False
        if s_isflush and o_isflush:
            return sorted(self.values, reverse=True) > sorted(other.values, reverse=True)
        if s_isflush:
            return True
        if o_isflush:
            return False
        if s_straight or o_straight:
            return s_straight > o_straight
        if s_three > o_three:
            return True
        if s_three < o_three:
            return False
        if len(s_pairs) > len(o_pairs):
            return True
        if len(s_pairs) < len(o_pairs):
            return False
        if s_pairs > o_pairs:
            return True
        if s_pairs < o_pairs:
            return False
        return sorted(self.values, reverse=True) > sorted(other.values, reverse=True)

def duals():
    with open('pb0054.txt') as f:
        for hands in f.readlines():
            cards = hands.split()
            yield Hand(cards[:5]), Hand(cards[5:])

print sum(1 for hand1, hand2 in duals() if hand1 > hand2)

print time() - start
