#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from time import time

start = time()

def farey(order):
    a, b, c, d = 0, 1, 1, order
    while c < order:
        k = (order + b)/d
        e = k*c - a
        f = k*d - b
        a, b, c, d = c, d, e, f
        yield a, b

counter = 0
farey_ = farey(12000)

while farey_.next() != (1, 3):
    continue
while farey_.next() != (1, 2):
    counter += 1

print counter
print time() - start
