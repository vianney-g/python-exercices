#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com
Doc : http://fr.wikipedia.org/wiki/Partition_d%27un_entier#Par_une_fonction_r.C3.A9cursive
"""


def memo(f):
    class Memo(dict):
        def __call__(self, *args):
            return self[args]
        def __missing__(self, key):
            r = self[key] = f(*key)
            return r
    return Memo()


def pentagons(n):
    k = p = 1
    while p <= n:
        yield k, p
        k = (-k) if k > 0 else (-k + 1)
        p = k*(3*k - 1)/2


@memo
def modulo1000000summations(n):
    if n == 1 or n == 0:
        return 1
    sum_item = lambda k, p: (-1)**(k + 1)*modulo1000000summations(n - p)
    return sum(sum_item(k, p) for k, p in pentagons(n)) % 1000000

i = 1
while(True):
    if modulo1000000summations(i) == 0:
        print i
        break
    i += 1
