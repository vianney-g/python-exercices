#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from itertools import combinations
from time import time

start = time()


def memo(f):
    class Memo(dict):
        def __missing__(self, k):
            r = self[k] = f(k)
            return r
    return Memo().__getitem__


def primes():
    n = 2
    tested = {}
    while 1:
        if n not in tested:
            yield n
            tested[n*n] = [n, ]
        else:
            for t in tested[n]:
                tested.setdefault(t+n, []).append(t)
            del tested[n]
        n += 1


@memo
def isprime(n):
    if n % 2 == 0:
        return False
    for d in xrange(3, int(n**0.5)+1, 2):
        if n % d == 0:
            return False
    return True

pcombinate = lambda p, q: isprime(int(str(p) + str(q)))


@memo
def is_prime_pair_set(pset):
    if len(pset) == 2:
        return pcombinate(pset[0], pset[1]) and pcombinate(pset[1], pset[0])
    if is_prime_pair_set(pset[:-1]):
        for p in pset[:-1]:
            if not is_prime_pair_set((p, pset[-1])):
                return False
        return True
    return False


def prime_pair_set(lg):
    pgen, ptab = primes(), []
    pgen.next()  # 2 is useless
    while 1:
        p = pgen.next()
        for psel in combinations(ptab, lg-1):
            if is_prime_pair_set(psel + (p, )):
                return psel + (p, )
        ptab.append(p)
        print ptab

print prime_pair_set(5)
print time() - start
