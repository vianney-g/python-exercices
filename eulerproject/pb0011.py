#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from operator import mul
from numpy import matrix

def products4(line):
    if len(line) < 4:
        return [0,]
    return [reduce(mul, line[i:i+4]) for i in xrange(len(line)-3)]

maxbyline = lambda grid: [max(products4(line)) for line in grid.tolist()]

def maxbydiag(grid):
    res = []
    for offset in xrange(-grid.shape[0], grid.shape[0]):
        res.append(max(maxbyline(grid.diagonal(offset))))
    return res

grid = []
while True:
    try:
        grid.append([int(n) for n in raw_input().split()])
    except EOFError:
        break
grid = matrix(grid)

maxh = max(maxbyline(grid))
maxv = max(maxbyline(grid.transpose()))
maxr = max(maxbydiag(grid))
maxl = max(maxbydiag(grid[::-1]))

print maxh, maxv, maxr, maxl


