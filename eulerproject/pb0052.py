#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

n = 1

sortit = lambda n: ''.join(sorted([c for c in str(n)]))
while 1:
    if sortit(n) == sortit(2*n) == sortit(3*n) == sortit(4*n) == sortit(5*n) == sortit(6*n):
        print n
        break
    n += 1

