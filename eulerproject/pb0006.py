#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

s = sum(xrange(1, 101))
print s*s - sum(x*x for x in xrange(1, 101))
