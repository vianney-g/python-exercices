#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

follower = lambda n: sum(int(s)**2 for s in str(n))


def memo(f):
    class Memo(dict):
        def __missing__(self, k):
            r = self[k] = f(k)
            return r
    return Memo().__getitem__


@memo
def finalint(n):
    if n == 89 or n == 1:
        return n
    return finalint(follower(n))

sum89 = 0
for i in xrange(1, 10000001):
    if finalint(i) == 89:
        print i
        sum89 += 1

print sum89
