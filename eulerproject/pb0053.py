#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from math import factorial as f

nCr = lambda n,r: f(n)/(f(r)*f(n-r))

onemillions = 0
for n in xrange(1,101):
    for r in xrange(1,n+1):
        onemillions += 1 if nCr(n,r) > 1000000 else 0

print onemillions
