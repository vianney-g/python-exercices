#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

# sum of primes below 2 millions

def isprime(n):
    if (n % 3 == 0) or (n % 5 == 0):
        return False
    for d in xrange(6, int(n**0.5)+ 1):
        if n % d == 0:
            return False
    return True

def primes():
    yield 2
    yield 3
    yield 5
    n = 7
    while n:
        if isprime(n):
            yield n
        n += 2

p = primes()
n = 0
mysum = 0
while True:
    n = p.next()
    if n > 2000000:
        break
    mysum += n
    print n

print mysum
