#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def spiral_corners_and_size():
    offset, size = 1, 1
    while 1:
        size += 2
        corners = [offset + i*(size-1) for i in xrange(1, 5)]
        offset = corners[-1]
        yield corners, size

def isprime(n):
    for d in xrange(3, int(n**0.5) + 1, 2):
        if n%d == 0:
            return False
    return True

corners_nb = primes_nb = 0.0
for corners, size in spiral_corners_and_size():
    primes_nb += len([c for c in corners if isprime(c)])
    corners_nb += 4
    if primes_nb/corners_nb < 0.1:
        print size - 2
        break




