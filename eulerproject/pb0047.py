#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""


def primaryfactors(n):
    pf = []
    d = 2
    while n != 1:
        while n % d == 0:
            if d in pf:
                pf[-1] *= d
            else:
                pf.append(d)
            n /= d
        d += 1
    return pf

n = 2*3*5*7
consecutives = []
while 1:
    pf = set(primaryfactors(n))
    if len(pf) == 4:
        print n
        for i, (m, pfm) in enumerate(consecutives[::-1], 0):
            if pf.intersection(pfm):
                consecutives = consecutives[-i:]
                break
        consecutives.append((n, pf))
        if len(consecutives) == 4:
            break
    else:
        consecutives = []
    n += 1

print consecutives
