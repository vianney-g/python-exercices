#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

print sum(i for i in xrange(1000) if i%3 == 0 or i%5 == 0)
