#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""


def memo(f):
    class Memo(dict):
        def __call__(self, *args):
            return self[args]
        def __missing__(self, key):
            r = self[key] = f(*key)
            return r
    return Memo()


def summations_counter(n):
    return sum(possibilities(n, start) for start in xrange(1, n + 1))


@memo
def possibilities(n, start):
    if start > n:
        return 0
    if start == 1 or start == n:
        return 1
    return sum(possibilities(n - start, s) for s in xrange(1, start + 1))

print summations_counter(100) - 1
