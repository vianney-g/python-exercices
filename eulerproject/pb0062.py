#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

n, permutations = 0, dict()
while True:
    n += 1
    key = ''.join(sorted(str(n**3)))
    try:
        permutations[key].append(n**3)
        if len(permutations[key]) == 5:
            print permutations[key][0]
            break
    except KeyError:
        permutations[key] = [n**3, ]
