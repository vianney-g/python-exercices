#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def memo(f):
    class Memo(dict):
        def __missing__(self, key):
            r = self[key] = f(key)
            return r
    return Memo().__getitem__

@memo
def permutations(mystring):
    if len(mystring) == 1:
        return [mystring,]
    res = []
    for i,x in enumerate(mystring):
        res.extend(x + p for p in permutations(mystring[0:i] + mystring[i+1:]))
    return res

print permutations('0123456789')[999999]

