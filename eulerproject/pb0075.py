#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from fractions import gcd


def primitive_pythagore_triplets():
    def relativeprimes(n):
        start = 2 if n % 2 else 1
        for p in xrange(start, n, 2):
            if gcd(p, n) == 1:
                yield p
    m = 2
    while 1:
        for n in relativeprimes(m):
            yield m*m - n*n, 2*m*n, m*m + n*n
        m += 1

limit = 1500000
ppt = primitive_pythagore_triplets()
triplet_count = limit*[0, ]
L = sum(ppt.next())

while L <= 2*limit:
    print L
    k = 1
    while 1:
        try:
            triplet_count[k*L - 1] += 1
            k += 1
        except IndexError:
            break
    L = sum(ppt.next())

print sum(1 for n in triplet_count if n == 1)
