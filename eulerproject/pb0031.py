#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def moneyways(amount, coins):
    if not (amount and coins):
        return 0
    count = 0
    for coin_amount in xrange(0, amount + 1, coins[0]):
        count += 1 if coin_amount == amount else moneyways(amount - coin_amount, coins[1:])
    return count

print moneyways(200, (1, 2, 5, 10, 20, 50, 100, 200))

