#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def memo(f):
    class Memo(dict):
        def __missing__(self, key):
            r = self[key] = f(key)
            return r
    return Memo().__getitem__

def triangular_numbers():
    i = a = 0
    while 1:
        i += 1
        a += i
        yield a

@memo
def factors(n):
    for d in xrange(2, int(n**0.5) + 1):
        if n % d == 0:
            pre_fact = factors(n/d)
            return pre_fact.union(d*a for a in pre_fact)
    return set((1,n))

for n in triangular_numbers():
    fact = factors(n)
    if len(fact) > 500:
        break

print fact
print n
