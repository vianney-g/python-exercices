#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def isprime(n):
    if n == 1:
        return True
    for d in xrange(2, int(n**0.5)+1):
        if not n%d:
            return False
    return True

def doublesquares():
    n = 1
    while 1:
        yield 2*n*n
        n += 1

def compositeodds():
    n = 9
    while 1:
        if not isprime(n):
            yield n
        n += 2

def goldbach_fail():
    for n in compositeodds():
        for m in doublesquares():
            if m >= n:
                return n
            if isprime(n - m):
                break

print goldbach_fail()
