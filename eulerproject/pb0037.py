#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def isprime(n):
    if n==1:
        return False
    for d in xrange(2, int(n**0.5)+ 1):
        if n % d == 0:
            return False
    return True

def primes(start=1):
    if start < 2:
        yield 1
        start += 1
    if start < 3:
        yield 2
        start += 1
    n = start if start%2 else start + 1
    while 1:
        if isprime(n):
            yield n
        n += 2

def istruncatable(n):
    n = str(n)
    for i in xrange(1,len(n)):
        if not (isprime(int(n[i:])) and isprime(int(n[:-i]))):
            return False
    return True

truncatables = []
for n in primes(11):
    if istruncatable(n):
        truncatables.append(n)
        if len(truncatables) == 11:
            break

print sum(truncatables)
