#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def rec_decimals(n):
    if n in (0,1):
        return ''
    rest, decimals = 1, []
    while True:
        dec, rest = 10*rest/n, 10*rest%n
        if not rest:
            return ''
        if (dec, rest) in decimals:
            rec_pattern = decimals[decimals.index((dec, rest)):]
            return ''.join(str(r[0]) for r in rec_pattern)
        decimals.append((dec, rest))

lng =[len(rec_decimals(n)) for n in xrange(1001)]
print lng.index(max(lng))
