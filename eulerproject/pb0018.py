#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Calcul du chemin de plus gros poids dans un triangle

"""

def routemax(datas):
    while len(datas) > 1:
        lastline = datas.pop()
        datas[-1] = [max(lastline[i:i+2]) + k for i, k in enumerate(datas[-1])]
    return datas[0][0]

datas = []
while True:
    try:
        datas.append([int(i) for i in raw_input().split()])
    except EOFError:
        break

print routemax(datas)
