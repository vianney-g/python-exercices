#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def memo(f):
    class Memo(dict):
        def __missing__(self, key):
            r = self[key] = f(key)
            return r
    return Memo().__getitem__

@memo
def nbpath((length, width)):
    if width > length:
        return nbpath((width, length))
    if width == 1:
        return length + 1
    return nbpath((length, width - 1)) + nbpath((length - 1, width))

square_nbpath = lambda n: nbpath((n,n))
print square_nbpath(40)
