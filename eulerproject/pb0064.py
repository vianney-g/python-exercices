#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""


def squareroot_period(n):
    a0 = int(n**0.5)
    if a0*a0 == n:
        return
    m, d, a = 0, 1, a0
    triplet_history = []
    while 1:
        m2 = d*a - m
        d2 = (n - m2**2)/d
        a2 = (a0 + m2)/d2
        if (m2, d2, a2) in triplet_history:
            break
        m, d, a = m2, d2, a2
        triplet_history.append((m, d, a))
        yield a

oddperiods = 0
for N in xrange(1, 10001):
    sp = [a for a in squareroot_period(N)]
    if len(sp) % 2:
        oddperiods += 1
print oddperiods
