#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
import sys

sys.setrecursionlimit(10000)

def memo(f):
    class Memo(dict):
        def __missing__(self, key):
            r = self[key] = f(key)
            return r
    return Memo().__getitem__

@memo
def lencollatz(n):
    if n == 1:
        return 1
    else:
        m = (3*n + 1) if n % 2 else n/2
        return lencollatz(m) + 1

maxlen = 0
res = 0
for k in xrange(1,1000000):
    if lencollatz(k) > maxlen:
        maxlen = lencollatz(k)
        res = k

print res
