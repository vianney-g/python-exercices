#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def e_sequence():
    yield 2
    yield 1
    k = 1
    while 1:
        yield k*2
        yield 1
        yield 1
        k += 1

def e_approximation():
    e_seq = e_sequence()
    h1, h2, k1, k2 = 1, 0, 0, 1
    while 1:
        a = e_seq.next()
        h1, h2 = a*h1 + h2, h1
        k1, k2 = a*k1 + k2, k1
        yield h1, k1

e = e_approximation()
for _ in range(100):
    num, den = e.next()
print sum(int(c) for c in str(num))
