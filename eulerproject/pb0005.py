#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from operator import mul

def smallest_fact_all(n):
    res = [2,3]
    for i in xrange(4,n+1):
        if reduce(mul, res) % i:
            res = [k for k in res if i % k]
            res.append(i)
    return res

print reduce(mul, smallest_fact_all(20))

