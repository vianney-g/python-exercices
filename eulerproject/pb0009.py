#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def triplets1000():
    for a in xrange(1,997):
        for b in xrange(2, 998):
            yield(a, b, 1000 - a - b)

for a, b, c in triplets1000():
    if (a*a + b*b) == c*c:
        print a, b, c
        print a*b*c
        break
