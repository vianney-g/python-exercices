#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

class PrimesFactory(object):

    def __init__(self):

        def primes_gen():
            n = 2
            tested = {}
            while 1:
                if n not in tested:
                    yield n
                    tested[n*n] = [n, ]
                else:
                    for t in tested[n]:
                        tested.setdefault(t+n, []).append(t)
                    del tested[n]
                n += 1

        self.primes_gen = primes_gen()
        self.primes = [self.primes_gen.next(), ]

    def get_until(self, limit):
        while(self.primes[-1] <= limit):
            self.primes.append(self.primes_gen.next())
        idx_limit = next(i for i, v in enumerate(self.primes) if v > limit)
        return self.primes[:idx_limit]

PF = PrimesFactory()


def summations_counter(n):
    ways = [1, ] + [0, ]*n
    for i, p in enumerate(PF.get_until(n)):
        for j in xrange(p, n + 1):
            ways[j] += ways[j - p]

    return ways[n]

TARGET = 5000
i = 10
while(True):
    if summations_counter(i) > TARGET:
        print i
        break
    i += 1
