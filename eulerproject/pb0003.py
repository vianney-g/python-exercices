#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

n = 600851475143

def primaryfactors(n):
    d = 2
    while n != 1:
        if n % d == 0:
            yield d
            n /= d
        d += 1

print [k for k in primaryfactors(n)][-1]
