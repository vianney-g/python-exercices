#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def isprime(n):
    for d in xrange(2, int(n**0.5) + 1):
        if n % d == 0:
            return False
    return True

def takeatour(n):
    n = str(n)
    for _ in xrange(len(n)):
        yield int(n)
        n = n[1:] + n[0]

def circulars(limit=1000000):
    yield 2
    for n in xrange(3,limit+1, 2):
        for m in takeatour(n):
            if not isprime(m):
                break
        else:
            yield n

print len([c for c in circulars()])

