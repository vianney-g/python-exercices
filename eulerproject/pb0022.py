#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from string import uppercase

datas = ''
while True:
    try:
        datas += raw_input()
    except EOFError:
        break

score = lambda name, pos: sum(uppercase.index(c) + 1 for c in name)*pos

datas = [name.strip()[1:-1] for name in datas.split(',')]
datas.sort()
print sum(score(name, pos + 1) for pos, name in enumerate(datas))
