#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

limit = 3/7.

closed, the_denominator, the_numerator = 0, 0, 0
for denominator in xrange(1,1000001):
    numerator = 3*denominator/7
    if denominator%7 == 0:
        numerator -= 1
    if float(numerator)/denominator > closed:
        closed = float(numerator)/denominator
        the_numerator, the_denominator = numerator, denominator

print the_numerator, the_denominator
