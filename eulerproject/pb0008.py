#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from operator import mul

def products5(nstr):
    for k in xrange(len(nstr) - 5):
        yield reduce(mul, (int(n) for n in nstr[k:k+5]))

nstr = ''
while True:
    try:
        nstr += raw_input()
    except EOFError:
        break
print max(p for p in products5(nstr))
