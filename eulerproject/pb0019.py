#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from calendar import weekday

sundaysnb = 0
for year in xrange(1901, 2001):
    sundaysnb += sum(1 for m in xrange(1,13) if weekday(year, m, 1) == 6)

print sundaysnb
