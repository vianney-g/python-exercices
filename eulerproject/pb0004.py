#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def palindroms():
    for i in xrange(1000, 99, -1):
        for j in xrange(1000, 99, -1):
            ixj = str(i*j)
            if ixj == ixj[::-1]:
                yield i*j

print max(palindroms())
