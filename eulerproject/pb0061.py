#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from time import time

start = time()


def triangles():
    n = 0
    while 1:
        n += 1
        yield n*(n + 1)/2


def squares():
    n = 0
    while 1:
        n += 1
        yield n*n


def pentagonals():
    n = 0
    while 1:
        n += 1
        yield n*(3*n - 1)/2


def hexagonals():
    n = 0
    while 1:
        n += 1
        yield n*(2*n - 1)


def heptagonals():
    n = 0
    while 1:
        n += 1
        yield n*(5*n - 3)/2


def octogonals():
    n = 0
    while 1:
        n += 1
        yield n*(3*n - 2)

are_chain = lambda n, m: str(n)[-2:] == str(m)[:2]


def four_digits(iterator):
    for t in iterator():
        if t > 9999:
            break
        if t > 999 and str(t)[2] != '0':  # 0 as a third digit is useless
            yield t

polynoms = {
    'tria': set(n for n in four_digits(triangles)),
    'squa': set(n for n in four_digits(squares)),
    'pent': set(n for n in four_digits(pentagonals)),
    'hexa': set(n for n in four_digits(hexagonals)),
    'hept': set(n for n in four_digits(heptagonals)),
    'octo': set(n for n in four_digits(octogonals)),
}


def validchains(beforeset, afterset, rest):
    for before in beforeset:
        for after in afterset:
            bbefore = {n for n in rest if are_chain(n, before)}
            aafter = {n for n in rest if are_chain(after, n)}
            for bb in bbefore:
                for aa in aafter:
                    lastone = int(str(aa)[-2:] + str(bb)[:2])
                    if lastone in rest:
                        yield [after, aa, lastone, bb, before]


def chainedset():

    def check_representation(int_list, pol_keys=('tria', 'squa', 'pent', 'hexa', 'hept')):
        if not pol_keys:
            return True
        pol_keys = list(pol_keys)
        k = pol_keys.pop()
        for i in int_list:
            if i in polynoms[k]:
                int_list.remove(i)
                return check_representation(int_list, tuple(pol_keys))
        return False

    for o in polynoms['octo']:
        rest = polynoms['tria'].union(polynoms['squa'].union(polynoms['pent'].union(polynoms['hept'])))
        octo_before = {n for n in rest if are_chain(n, o)}
        octo_after = {n for n in rest if are_chain(o, n)}
        for vc in validchains(octo_before, octo_after, rest):
            if check_representation(vc[:]):
                return vc + [o, ]

cs = chainedset()
print cs, sum(cs)
print time() - start
