#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def fibo():
    a = b = 1
    yield a
    while True:
        yield b
        a, b = b, a+b

for i,n in enumerate(fibo()):
    if len(str(n)) > 999:
        print i+1
        break
