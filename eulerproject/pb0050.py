#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def memo(f):
    class Memo(dict):
        def __missing__(self, k):
            r = self[k] = f(k)
            return r
    return Memo().__getitem__

def primes_gen():
    yield 1
    n = 2
    tested = {}
    while 1:
        if n not in tested:
            yield n
            tested[n*n] = [n,]
        else:
            for t in tested[n]:
                tested.setdefault(t+n, []).append(t)
            del tested[n]
        n += 1

@memo
def isprime(n):
    if n%2 == 0:
        return False
    for d in xrange(3, int(n**0.5)+1, 2):
        if n%d == 0:
            return False
    return True

def prime_sum_of_n_primes(n, stop=1000000):
    primes = primes_gen()
    seave = [primes.next() for _ in xrange(n)]
    while 1:
        mysum = sum(seave)
        if mysum > stop:
            return None
        if isprime(mysum):
            return mysum
        seave = seave[1:] + [primes.next(),]

n = 7
while 1:
    k = prime_sum_of_n_primes(n)
    if k:
        print 'current max prime: %i for n=%i' % (k,n)
    else:
        pass
        #print 'no solution for n=%i' % (n,)
    n += 1

