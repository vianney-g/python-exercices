#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
from operator import mul

def decparts():
    n = len_ = 0
    while 1:
        n += 1
        len_ += len(str(n))
        yield n, len_

#searchfor = [12, 10, 1]
searchfor = [1000000, 100000, 10000, 1000, 100, 10, 1]

r = searchfor.pop()
digits = []
for n, len_ in decparts():
    if not r:
        break
    while r <= len_:
        digits.append(int(str(n)[-1-len_+r]))
        r = 0
        try:
            r = searchfor.pop()
        except IndexError:
            break

print digits
print reduce(mul, digits)
