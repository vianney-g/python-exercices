#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from itertools import permutations

def isprime(n):
    for d in xrange(2, int(n**0.5)+1):
        if not n%d:
            return False
    return True

def pandigit_primes(len_):
    for p in permutations('987654321'[9-len_:]):
        if isprime(int(''.join(p))):
            yield int(''.join(p))

maxi = None
for i in xrange(7,0,-1):
    try:
        maxi =  pandigit_primes(i).next()
    except StopIteration:
        pass
    finally:
        if maxi:
            break

print maxi
