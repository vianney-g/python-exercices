#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

def memo(f):
    class Memo(dict):
        def __missing__(self, key):
            r = self[key] = f(key)
            return r
    return Memo().__getitem__

@memo
def factors(n):
    for d in xrange(2, n/2 + 1):
        if n % d == 0:
            pre_fact = factors(n/d)
            return pre_fact.union(d*a for a in pre_fact)
    return set((1,n))

def amicables(maxnb=10000):
    for n in xrange(2, maxnb + 1):
        sf = sum(factors(n)) - n
        if sf < n and (sum(factors(sf)) - sf) == n:
            yield sf
            yield n

print sum(amicables())
