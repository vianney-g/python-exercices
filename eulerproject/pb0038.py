#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

DIGITS = set('123456789')

def n_pandigitals(n):
    muls = range(1,n+1)
    k = 1
    while 1:
        concat = ''.join(str(k*i) for i in muls)
        if len(concat) > 9:
            break
        if len(concat) == 9:
            if not DIGITS.difference(concat):
                yield int(concat)
        k += 1

maxs = []
for mul in xrange(2, 10):
    maxs.extend([p for p in n_pandigitals(mul)])

print max(maxs)



