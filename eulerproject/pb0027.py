#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
def memo(f):
    class Memo(dict):
        def __missing__(self, key):
            r = self[key] = f(key)
            return r
    return Memo().__getitem__

@memo
def isprime(n):
    for d in xrange(2, int(n**0.5) + 1):
        if n % d == 0:
            return False
    return True

def maxi_primes():
    for a in xrange(-1000, 1001):
        for b in xrange(-999, 1001, 2):
            n = 0
            while True:
                if not isprime(abs(n*n + a*n + b)) and n:
                    yield (n, a, b)
                    break
                n += 1

print 'please wait...'
max_score = max(score for score in maxi_primes())
print max_score
print max_score[1]*max_score[2]

