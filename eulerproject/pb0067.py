#!/usr/bin/env python
# *-* coding:utf-8 *-*


triangle = []
while 1:
    try:
        triangle.append([int(n) for n in raw_input().split()])
    except EOFError:
        break

while len(triangle) > 1:
    lastline = triangle.pop()
    for i in xrange(len(triangle[-1])):
        triangle[-1][i] += max(lastline[i:i+2])

print triangle

