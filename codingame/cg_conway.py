#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date : 2013-03-27
Author : Vianney Gremmel loutre.a@gmail.com

"""

firstline = line = raw_input()
lmax = int(raw_input())


def nextline(line):
    line = line[0:-len(firstline)]
    samechar = lambda s: len(s) - len(s.lstrip(s[0]))
    nextline = ""
    while len(line) != 0 :
        sc = samechar(line)
        nextline += str(sc) + line[0]
        line = line[sc:]
    if len(nextline) == 0 :
        nextline = "1"
    return nextline + firstline

for i in xrange(lmax-1):
    line = nextline(line)
    print line

print " ".join(line)
