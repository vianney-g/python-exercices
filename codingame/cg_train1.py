#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
n = [int(n) for n in raw_input().split()]
s = len(raw_input())
w = raw_input().split()

print sum(n) == s and w[0] or w[1]

