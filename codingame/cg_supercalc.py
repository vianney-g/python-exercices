#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

n = int(raw_input())

tasks = []
for _ in xrange(n):
    tasks.append([int(t) for t in raw_input().split()])

# sort task depending on the end date
tasks.sort(key=sum)

def scheduledtasks(tasks):
    day = 0
    for t in tasks:
        if t[0] >= day:
            yield t
            day = sum(t)

print sum(1 for _ in scheduledtasks(tasks))
