#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

n = int(raw_input())

def lines(n) :
    for _ in xrange(n):
        try :
            yield raw_input()
        except EOFError :
            break

def formatline(line):
    tab = ""
    line = line.strip()
    quoteholder = False
    for c, d in zip(line, line[1:] + " "):
        if quoteholder and c != "'":
            yield c
        elif c == "(":
            yield c
            yield "\n"
            tab += 4*" "
            yield tab
        elif c == ")":
            yield "\n"
            tab = tab[4:]
            yield tab
            yield c
        elif c == ";":
            if d != ")":
                yield c
            yield "\n"
            yield tab
        elif c == "'":
            quoteholder = not quoteholder
            yield c
        elif c != " " or quoteholder:
            yield c

res = ""
for line in lines(n):
    for c in formatline(line):
        res += c

print res.rstrip()

