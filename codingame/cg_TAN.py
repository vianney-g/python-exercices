#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

import heapq
from math import cos, sqrt

depart = raw_input()
arrivee = raw_input()
nbstops = int(raw_input())
stops = {}
for _ in xrange(nbstops):
    s = raw_input().split(',')
    stops[s[0]] = s[1:]

def graph():
    x_pos = lambda lg_a, lg_b, a, b: (lg_b - lg_a)*cos((a + b)/2)
    y_pos = lambda a, b: a - b
    dist = lambda x,y: sqrt(x*x + y*y)*6371

    G = {}
    nblinks = int(raw_input())
    for _ in xrange(nblinks):
        s1, s2 = raw_input().split()
        lg_a, lt_a = map(float, stops[s1][2:4])
        lg_b, lt_b = map(float, stops[s2][2:4])
        dt = dist(x_pos(lg_a, lg_b, lt_a, lt_b), y_pos(lt_a, lt_b))
        try:
            G[s1][s2] = dt
        except KeyError:
            G[s1] = {s2: dt}
    return G

def shortestpath(G, start, end):
    def flatten(L):       # Flatten linked list of form [0,[1,[2,[]]]]
        while L:
            yield L[0]
            L = L[1]

    q = [(0, start, ())]  # Heap of (cost, path_head, path_rest).
    visited = set()       # Visited vertices.
    while True:
        try:
            (cost, v1, path) = heapq.heappop(q)
        except IndexError:
            return False
        if v1 not in visited:
            visited.add(v1)
            if v1 == end:
                return list(flatten(path))[::-1] + [v1]
            path = (v1, path)
            for (v2, cost2) in G[v1].iteritems():
                if v2 not in visited:
                    heapq.heappush(q, (cost + cost2, v2, path))

sp = shortestpath(graph(), depart, arrivee)
print '\n'.join(stops[s][0][1:-1] for s in sp) if sp else 'IMPOSSIBLE'
