#!/usr/bin/env python
# *-* coding:utf-8 *-*

n = int(raw_input())
msg = raw_input()

dicSMS = {
    '1' : '1',
    '2' : 'a',
    '22' : 'b',
    '222' : 'c',
    '2222' : '2',
    '3' : 'd',
    '33' : 'e',
    '333' : 'f',
    '3333' : '3',
    '4' : 'g',
    '44' : 'h',
    '444' : 'i',
    '4444' : '4',
    '5' : 'j',
    '55' : 'k',
    '555' : 'l',
    '5555' : '5',
    '6' : 'm',
    '66' : 'n',
    '666' : 'o',
    '6666' : '6',
    '7' : 'p',
    '77' : 'q',
    '777' : 'r',
    '7777' : 's',
    '77777' : '7',
    '8' : 't',
    '88' : 'u',
    '888' : 'v',
    '8888' : '8',
    '9' : 'w',
    '99' : 'x',
    '999' : 'y',
    '9999' : 'z',
    '99999' : '9',
    '0' : ' ',
    '00' : '0',
}

MAXLEN = 160

def decodeSMS(msg) :
    """ generator : yield a letter as soon as it's valid """
    prevholder = ''
    msglen = 0
    for cur in msg:
        if msglen == MAXLEN:
            return
        if len(prevholder) == 0 or cur == prevholder[-1]:
            if (prevholder + cur) in dicSMS.keys():
                prevholder += cur
            else:
                prevholder = cur
        else:
            try:
                yield dicSMS[prevholder]
                msglen += 1
            except KeyError:
                pass
            prevholder = cur
    # the last one...
    try:
        yield dicSMS[prevholder]
    except KeyError:
        pass

print ''.join(c for c in decodeSMS(msg))

