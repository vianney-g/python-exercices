#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""
n = int(raw_input())

if n:
    temps = [int(t) for t in raw_input().split()]
    tamin = min([abs(t) for t in temps])
    print tamin in temps and tamin or -tamin
else:
    print 0

