#!/usr/bin/env python

from itertools import permutations

counter = raw_input() #useless

def actg_sequences():
    while True:
        try:
            yield raw_input()
        except EOFError:
            break

def smaller_concatenation(sequences):
    if not sequences:
        return ''
    if len(sequences) == 1:
        return sequences[0]
    seq1, seq2 = sequences[0], sequences[1]
    sieve_len = len(seq1) if len(seq1)<len(seq2) else len(seq2)
    concatenation = seq1 + seq2
    if seq1 in seq2:
        concatenation = seq2
    elif seq2 in seq1:
        concatenation = seq1
    else:
        while sieve_len:
            if seq1[-sieve_len:] == seq2[:sieve_len]:
                concatenation = seq1 + seq2[sieve_len:]
                break
            sieve_len -= 1
    return smaller_concatenation(tuple([concatenation,] + list(sequences[2:])))

sequences = [s for s in actg_sequences()]
minimal_seq = min((smaller_concatenation(s) for s in permutations(sequences)), key=len)
print len(minimal_seq)

