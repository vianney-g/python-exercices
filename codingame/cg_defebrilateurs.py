#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

from math import cos, sqrt

tof = lambda s : float(s.replace(',', '.'))

lgt = tof(raw_input())
lat = tof(raw_input())
N = int(raw_input())

def defebltrs():
    for _ in xrange(N):
        d = raw_input().split(';')
        d[4] = tof(d[4])
        d[5] = tof(d[5])
        yield d

x_pos = lambda lg_a, lg_b, a, b: (lg_b - lg_a)*cos((a + b)/2)
y_pos = lambda a, b: a - b
dist = lambda x,y: sqrt(x*x + y*y)*6371
dmin = -1
feb = None

for d in defebltrs():
    dt = dist(x_pos(d[4], lgt, d[5], lat), y_pos(d[5], lat))
    if (dmin < 0 or dt < dmin):
        dmin = dt
        feb = d

print feb[1] if feb else 'no items found'



