#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

CONV= ('00', '0')
msg = raw_input()

def chucknorriser(msgbin):
    prev = ''
    for c in msgbin:
        if c != prev:
            yield ' ' + CONV[int(c)] + ' '
            prev = c
        yield '0'

binarize = lambda c: bin(ord(c))[2:].zfill(7)
msgbin = "".join(binarize(c) for c in msg)
chuck = ''.join(c for c in chucknorriser(msgbin))

print chuck.strip()
