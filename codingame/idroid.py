#!/usr/bin/env python

class Tree(object):
    def __init__(self, value, ancestor=None):
        self.value = value
        self.ancestor = ancestor
        self.children = []

    def addbranch(self, branch):
        if branch:
            child = self.findchildren(branch[0])
            if not child:
                child = Tree(branch[0], self)
                self.children.append(child)
            child.addbranch(branch[1:])

    def findchildren(self, value):
        for child in self.children:
            if child.value == value:
                return child

    def size(self):
        return len(self.children) + sum(c.size() for c in self.children)

counter = raw_input() #useless

def phonenumbers():
    while True:
        try:
            yield raw_input()
        except EOFError:
            break

root = Tree(None)

for pn in phonenumbers():
    root.addbranch(pn)

print root.size()
