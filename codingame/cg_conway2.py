#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date : 2013-03-27
Author : Vianney Gremmel loutre.a@gmail.com

"""

start = int(raw_input())
lmax = int(raw_input())

def nextline(line):
    counter = 1
    nextline = []
    for i, c in enumerate(line):
        try:
            if c == line[i+1]:
                counter +=1
            else:
                nextline.extend([counter, c])
                counter = 1
        except IndexError:
            nextline.extend([counter, c])
    return nextline

line = [start]
for _ in xrange(lmax - 1):
    line = nextline(line)

print " ".join(str(n) for n in line)
