#!/usr/bin/env python
# *-* coding:utf-8 *-*

"""
Date :
Author : Vianney Gremmel loutre.a@gmail.com

"""

morse = raw_input()
nbwords= int(raw_input())

letters = {
    'A': '.-', 'B': '-...', 'C': '-.-.',
    'D': '-..', 'E': '.', 'F': '..-.',
    'G': '--.', 'H': '....', 'I': '..',
    'J': '.---', 'K': '-.-', 'L': '.-..',
    'M': '--', 'N': '-.', 'O': '---',
    'P': '.--.', 'Q': '--.-', 'R': '.-.',
    'S': '...', 'T': '-', 'U': '..-',
    'V': '...-', 'W': '.--', 'X': '-..-',
    'Y': '-.--', 'Z': '--..',
}

convword = lambda word: ''.join(letters[t] for t in word)
DICO = [convword(raw_input()) for _ in xrange(nbwords)]
MAXLEN = len(max(DICO, key=len))
MINLEN = len(min(DICO, key=len))

def nbmsgs(msg, nb=0):
    stop = min(MAXLEN, len(msg))
    for w in xrange(MINLEN -1, stop):
        word = msg[:w+1]
        if word in DICO:
            nextword = msg[w+1:]
            if nextword:
                nb = nbmsgs(nextword, nb)
            else:
                nb = nb + 1
    return nb

print nbmsgs(morse)
